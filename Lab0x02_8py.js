var Lab0x02_8py =
[
    [ "callback", "Lab0x02_8py.html#a29ba03287fda563290f1fc91569246a3", null ],
    [ "avg", "Lab0x02_8py.html#a21473e7268ae5d25047791e2fc7212dc", null ],
    [ "check", "Lab0x02_8py.html#ac1dc0a574ba78289b4b92626ee090fb5", null ],
    [ "extint", "Lab0x02_8py.html#a43d8577801277ac2de1f4eba4cf03066", null ],
    [ "isr_count", "Lab0x02_8py.html#a992f20698dce7002e0cc7388c7146c79", null ],
    [ "LEDPin", "Lab0x02_8py.html#ac76f44f02729555dc30e2831449d94eb", null ],
    [ "period", "Lab0x02_8py.html#afea4393f574c7af57b8411fa70eda692", null ],
    [ "prescaler", "Lab0x02_8py.html#a73b684724c8252ce26ae2cd0c437c555", null ],
    [ "setup", "Lab0x02_8py.html#ae51d2298eafc6dc58f7b413b4109a68d", null ],
    [ "start_time", "Lab0x02_8py.html#a6552642d71097afddc2a649e0fcee6b5", null ],
    [ "total", "Lab0x02_8py.html#aacd4405f6c041020759703818dfadd65", null ]
];